# microHAT SENSOR INTERFACE

This repository contains schematics, diagrams and instructions to create a sensor interface that is compliant with the microHAT specifications of the Raspberry Pi. This microHAT was designed with a terrarium owner as the user in mind. Appropriate user stories were applied alongside the collected requirements to develop the specifications for the microHAT. Given this use case, this device is not intended to be weatherproof, but is intended to operate for long periods of time without switching off. As such, the final design operates with USB port that can be attached to a power supply, so it is always on.

LTSPice software was used iteratively for simulations in the process of designing this device. KiCAD software was essential in designing the final PCB of the device.

## USING THIS PROJECT

The use case was broken up into three subsystems: power supply, amplifier and status LEDs. Subsequently, there are respective directories created in this project containing relevant simulation files, schematics and documents to understand the design and help in the process of manufacture. 

A Getting Started file contains important information to help you get started on connecting this device to the 
Raspberry Pi GPIO pins, while the Manufacturing Notes file contains important notes for manufacture.

Going through the respective subsystem directories first will give you a good grasp on understanding the project, before going through the Manufacturing Notes and PCB directories.

## HOW TO CONTRIBUTE TO THIS PROJECT

1. Report issues in the design by creating an issue request 
2. Suggest enhancements to the design by creating an issue request
