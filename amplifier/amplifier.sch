EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Amplifier"
Date "2021-06-05"
Rev "1"
Comp "Group 33"
Comment1 "Created by: Nthabiseng Tleane TLNNTH001"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LT6015xS5 U3
U 1 1 60BAA406
P 4450 5000
F 0 "U3" H 4794 5046 50  0000 L CNN
F 1 "LT6015xS5" H 4794 4955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4350 4800 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/601567ff.pdf" H 4450 5200 50  0001 C CNN
	1    4450 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R12
U 1 1 60BAC206
P 3900 4550
F 0 "R12" H 3959 4596 50  0000 L CNN
F 1 "100k" H 3959 4505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3900 4550 50  0001 C CNN
F 3 "~" H 3900 4550 50  0001 C CNN
	1    3900 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R11
U 1 1 60BAD7E2
P 3500 4900
F 0 "R11" V 3696 4900 50  0000 C CNN
F 1 "51.5k" V 3605 4900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3500 4900 50  0001 C CNN
F 3 "~" H 3500 4900 50  0001 C CNN
	1    3500 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 4900 3900 4900
Wire Wire Line
	3900 4900 3900 4650
Wire Wire Line
	3900 4450 3900 4250
Wire Wire Line
	4750 5000 5450 5000
Wire Wire Line
	4150 5100 3900 5100
Wire Wire Line
	3900 5100 3900 5650
Wire Wire Line
	3900 5650 5450 5650
Wire Wire Line
	5450 5650 5450 5000
Connection ~ 5450 5000
Wire Wire Line
	5450 5000 6050 5000
Wire Wire Line
	3900 4900 3600 4900
Connection ~ 3900 4900
Wire Wire Line
	3400 4900 2800 4900
Wire Wire Line
	2800 4900 2800 5100
$Comp
L power:+12V #PWR015
U 1 1 60BC2C74
P 5000 4200
F 0 "#PWR015" H 5000 4050 50  0001 C CNN
F 1 "+12V" H 5015 4373 50  0000 C CNN
F 2 "" H 5000 4200 50  0001 C CNN
F 3 "" H 5000 4200 50  0001 C CNN
	1    5000 4200
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR016
U 1 1 60BC3795
P 5750 4150
F 0 "#PWR016" H 5750 4250 50  0001 C CNN
F 1 "-12V" H 5765 4323 50  0000 C CNN
F 2 "" H 5750 4150 50  0001 C CNN
F 3 "" H 5750 4150 50  0001 C CNN
	1    5750 4150
	1    0    0    -1  
$EndComp
Text Label 4350 5300 2    50   ~ 0
V-
Text Label 4350 4700 0    50   ~ 0
V+
Text Label 5000 4200 2    50   ~ 0
V+
Text Label 5750 4150 2    50   ~ 0
V-
Text HLabel 2800 5100 0    50   Input ~ 0
IN
Text GLabel 3900 4250 0    50   Input ~ 0
GND
Text HLabel 6050 5000 2    50   Input ~ 0
OUT
$EndSCHEMATC
